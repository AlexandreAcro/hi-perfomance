﻿#include <algorithm>
#include <iostream>
#include <omp.h>
#include <fstream>
#include <vector>
#include <tuple>
#include <chrono>
#include <windows.h>


using namespace std;

const unsigned PARALLEL_CUTOFF = 256;

struct my_clock
{
    typedef double                             rep;
    typedef std::ratio<1>                      period;
    typedef std::chrono::duration<rep, period> duration;
    typedef std::chrono::time_point<my_clock>  time_point;
    static const bool is_steady =              false;

    static time_point now()
    {
        static const long long frequency = init_frequency();
        LARGE_INTEGER t;
        QueryPerformanceCounter(&t);
        return time_point(duration(static_cast<rep>(t.QuadPart)/frequency));
    }
private:
    static long long init_frequency()
    {
        LARGE_INTEGER f;
        QueryPerformanceFrequency(&f);
        return f.QuadPart;
    }
};


void qsort_parallel(int *l, int *r) {
	if (r - l <= 1) return;
	int z = *(l + (r - l) / 2);
	int *ll = l, *rr = r - 1;
	while (ll <= rr) {
		while (*ll < z) ll++;
		while (*rr > z) rr--;
		if (ll <= rr) {
			std::swap(*ll, *rr);
			ll++;
			rr--;
		}
	}

	if (rr - l < PARALLEL_CUTOFF) {
		qsort_parallel(l, rr + 1);
	} else {
	#pragma omp task
		{
			qsort_parallel(l, rr + 1);
		}
	}

	if (r - ll < PARALLEL_CUTOFF) {
		qsort_parallel(ll, r);
	} else {
	#pragma omp task
		{
			qsort_parallel(ll, r);
		}
	}
}


void quicksort(int *array, int len, int threadsCount) {
#pragma omp parallel num_threads(threadsCount)
	{
	#pragma omp single nowait
		{
			qsort_parallel(array, array + len);
		}
	}
}


int main() {
	omp_set_dynamic(0); //Prevent the system from dynamically changing the number of threads
	
	my_clock::time_point startTime, stopTime;

	ofstream fs("result.txt", ios_base::out);

	
	vector<tuple<unsigned, unsigned>> ranges = { { 2, 200 }, 
												{1000, 1010},
												{10000, 10010},
												{100000, 100010},
												{1000000, 1000010},
												{10000000, 10000010},
												{100000000, 100000010},
												{200000000, 200000010},
												{300000000, 300000010},
												{400000000, 400000010} };

	for (unsigned thc = 2; thc != 17 ; ++thc) {
		for (auto range : ranges) {
			for (size_t i = get<0>(range); i != get<1>(range); ++i) {
				int *arr = new int[i];
				for (size_t j = 0; j != i; j++) {
					arr[j] = rand();
				}

				startTime = my_clock::now();
				quicksort(arr, i, thc);
				stopTime = my_clock::now();

				delete[] arr;

				cout << thc << ' ' << i << endl;
				fs << thc << ';' << get<0>(range) << ';' << std::chrono::duration_cast<std::chrono::nanoseconds>(stopTime - startTime).count() << endl;
			}
			fs << endl;
			fs.flush();
		}
	}

	fs.close();

	cout << "ready" << endl;

	return 0;
}
