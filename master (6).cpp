#define _CRT_SECURE_NO_WARNINGS
#include <windows.h> 
#include <strsafe.h>
#include <iostream>

using namespace std;

#define BUFSIZE 512


int main(int argc, char *argv[]) {
	HANDLE hNamedPipe;
	DWORD  cbWritten;
	DWORD  cbRead;
	char   szBuf[BUFSIZE];

	LPCSTR lpszPipename = "\\\\.\\pipe\\Quest";

	printf("Client\n\n");

	hNamedPipe = CreateFileA(lpszPipename, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

	if (hNamedPipe == INVALID_HANDLE_VALUE) {
		fprintf(stdout, "CreateFile: Error %ld\n", GetLastError());
		return 0;
	}

	fprintf(stdout, "\nConnected.\n");

	while (1) {
		Sleep(1000);
		if (ReadFile(hNamedPipe, szBuf, BUFSIZE, &cbRead, NULL)) {
			if (szBuf[0] == '*') {
				printf("You won!\n");
				fflush(stdin);
				getchar();
				break;
			} else {
				if (szBuf[0] == '+') {
					printf("Correct answer!\n\n");
					printf("Next question: %s\n", szBuf + 1);
				} else if (szBuf[0] == '-') {
					printf("Wrong answer!\n\n");
					printf("Next question: %s\n", szBuf + 1);
				} else {
					printf("First question: %s\n", szBuf);
				}
			}
		} else {
			printf("You lost!\n");
			fflush(stdin);
			getchar();
			break;
		}

		std::cout << "Enter answer (lowercase)>";
		std::cin >> szBuf;

		if (!WriteFile(hNamedPipe, szBuf, strlen(szBuf) + 1, &cbWritten, NULL))
			break;

	}

	CloseHandle(hNamedPipe);
	return 0;
}
