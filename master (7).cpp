#define _CRT_SECURE_NO_WARNINGS
#include <windows.h> 
#include <stdio.h> 
#include <tchar.h>
#include <strsafe.h>
#include <iostream>

using namespace std;

#define BUFSIZE 512

DWORD WINAPI InstanceThread(LPVOID);

const char *qbase[][2] = { { "What sandwich did the author of this program eat today?", "common" }, 
                          { "How many fingers does the program show now?", "0" },
                          { "How long?", "enough" }, 
                          { "What is the sense of life?", "42" },
                          { "Is this statement not true?", "paradox" },
                          { "Do you need help?", "yes" },
                          { "How much is 0/0?", "uncertainty" },
                          { "Paper crying kerosene cactus throw shortness of breath?", "no" },
                          { "Where?", "here" }, 
                          { "Is black good?", "it depends" }
};

bool hasWon = false;
CRITICAL_SECTION critSection;

int main(void) {
    BOOL   fConnected = FALSE;
    DWORD  dwThreadId = 0;
    HANDLE hPipe = INVALID_HANDLE_VALUE, hThread = NULL;
    LPCSTR lpszPipename = "\\\\.\\pipe\\Quest";

    InitializeCriticalSection(&critSection);


    for (;;) {
        cout << "Pipe Server: Main thread awaiting client connection on " << lpszPipename << endl;
        hPipe = CreateNamedPipeA(
            lpszPipename,             
            PIPE_ACCESS_DUPLEX,      
            PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,                
            PIPE_UNLIMITED_INSTANCES, 
            BUFSIZE,                  
            BUFSIZE,                  
            INFINITE,                        
            NULL);                    

        if (hPipe == INVALID_HANDLE_VALUE) {
            cout << "CreateNamedPipe failed, GLE=" << GetLastError() << endl;
            return -1;
        }

        fConnected = ConnectNamedPipe(hPipe, NULL);

        if (fConnected) {
            cout << "Client connected, creating a processing thread." << endl;

            hThread = CreateThread(
                NULL,             
                0,                
                InstanceThread,    
                (LPVOID)hPipe,    
                0,                 
                &dwThreadId);

            if (hThread == NULL) {
                cout << "CreateThread failed, GLE=" << GetLastError() << endl;
                return -1;
            } else CloseHandle(hThread);
        } else
            CloseHandle(hPipe);
    }

    return 0;
}

DWORD WINAPI InstanceThread(LPVOID lpvParam)
{
    char *pchRequest = new char[BUFSIZE];
    char *pchReply = new char[BUFSIZE];;

    DWORD cbBytesRead = 0, cbReplyBytes = 0, cbWritten = 0;
    BOOL fSuccess = FALSE;
    HANDLE hPipe = (HANDLE)lpvParam;
    srand(reinterpret_cast<unsigned>(hPipe));

    char current_question = -1, score = 0;

    while (true) {
        EnterCriticalSection(&critSection);
        if (hasWon) {
            LeaveCriticalSection(&critSection);
            break;
        }

        if (current_question == -1) {
            current_question = rand() % 7;
            *pchReply = '\0';
            strcpy(pchReply, qbase[current_question][0]);
        } else if (!strncmp(qbase[current_question][1], pchRequest, BUFSIZE)) {
            if (++score == 2) {
                hasWon = true;
                *pchReply = '\0';
                strcpy(pchReply, "*");
            } else {
                current_question = rand() % 10;
                *pchReply = '\0';
                strcpy(pchReply, "+");
                strcat(pchReply, qbase[current_question][0]);
            }
        } else {
            current_question = rand() % 10;
            *pchReply = '\0';
            strcpy(pchReply, "-");
            strcat(pchReply, qbase[current_question][0]);
        }
        LeaveCriticalSection(&critSection);

        fSuccess = WriteFile(
            hPipe,      
            pchReply,    
            cbReplyBytes = strlen(pchReply) + 1,
            &cbWritten, 
            NULL);

        if (!fSuccess || cbReplyBytes != cbWritten) {
            cout << "InstanceThread WriteFile failed, GLE=" << GetLastError() << endl;
            break;
        }

        if (score == 2) break;

        fSuccess = ReadFile(
            hPipe,
            pchRequest,
            BUFSIZE,
            &cbBytesRead,
            NULL);

        if (!fSuccess || cbBytesRead == 0) {
            if (GetLastError() == ERROR_BROKEN_PIPE) {
                cout << "InstanceThread: client disconnected." << endl;
            } else {
                cout << "InstanceThread ReadFile failed, GLE=" << GetLastError() << endl;
            }
            break;
        }
    }

    FlushFileBuffers(hPipe);
    DisconnectNamedPipe(hPipe);
    CloseHandle(hPipe);

    delete[] pchRequest;
    delete[] pchReply;

    cout << "InstanceThread exiting." << endl;
    return 1;
}