#include <Windows.h>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
#include <vector>

using namespace std;

float *numbers, *rows;

HANDLE eventWfor;
CRITICAL_SECTION critSection;

unsigned procCount = 0, N = 0;
float minVal = ~0U, maxVal = 0;

DWORD WINAPI Worker(LPVOID ptr) {
	unsigned pos = (unsigned)ptr;
	float *matPos = numbers + pos * N;

	float avg = 0;
	for (unsigned i = 0; i != N; ++i, ++matPos) {
		if (*matPos > 0) {
			avg += *matPos;
		}
	}
	rows[pos] = avg;
	
	EnterCriticalSection(&critSection);
	if (minVal > avg) {
		minVal = avg;
	}
	if (maxVal < avg) {
		maxVal = avg;
	}
	if (!--procCount) {
		SetEvent(eventWfor);
	}
	LeaveCriticalSection(&critSection);
	return 0;
}

int main() {
	InitializeCriticalSection(&critSection);
	eventWfor = CreateEventA(NULL, TRUE, FALSE, NULL);

	if (!eventWfor) return -1;

	ifstream ifile("input.txt");

	unsigned K = 0;
	ifile >> K >> N;

	procCount = K;

	numbers = new float[N * K];
	rows = new float[K];

	{
		float *ptr = numbers;
		for (unsigned j = 0; j != K; ++j) {
			for (unsigned i = 0; i != N; ++i) {
				ifile >> *ptr++;
			}
			CloseHandle(CreateThread(NULL, 0, Worker, (void *)j, NULL, NULL));
		}
	}

	ifile.close();

	WaitForSingleObject(eventWfor, INFINITE);

	CloseHandle(eventWfor);
	DeleteCriticalSection(&critSection);
	delete[] numbers;

	ofstream ofile("output.txt");
	
	for (unsigned j = 0; j != K; ++j) {
		ofile << rows[j] << endl;
	}
	delete[] rows;

	ofile << endl << minVal + maxVal;
	ofile.close();

	return 0;
}
