#include <Windows.h>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
#include <vector>

using namespace std;

string csequence;
HANDLE mutex, eventInit;

struct worker_data {
	string olbstr, newstr;
	unsigned myIndex = 0;
};

unsigned currentWorker = 0;

DWORD WINAPI Worker(LPVOID ptr) {
	worker_data wdata = *(reinterpret_cast<worker_data *>(ptr));
	SetEvent(eventInit);

	while (true) {
		WaitForSingleObject(mutex, INFINITE);
		if (currentWorker == wdata.myIndex) {
			size_t start_pos = 0;
			while ((start_pos = csequence.find(wdata.olbstr, start_pos)) != std::string::npos) {
				csequence.replace(start_pos, wdata.olbstr.length(), wdata.newstr);
				start_pos += wdata.newstr.length();
			}

			++currentWorker;
			ReleaseMutex(mutex);
			return 0;
		}
		ReleaseMutex(mutex);

		SwitchToThread();
	}
}

int main() {
	mutex = CreateMutexA(NULL, FALSE, NULL);
	eventInit = CreateEventA(NULL, TRUE, FALSE, NULL);

	if (!(mutex && eventInit)) return -1;

	ifstream ifile("input.txt");

	vector<HANDLE> threads;

	if (getline(ifile, csequence)) {
		worker_data wdata;

		while (ifile >> wdata.olbstr >> wdata.newstr) {
			ResetEvent(eventInit);
			threads.push_back(CreateThread(NULL, 0, Worker, &wdata, NULL, NULL));
			WaitForSingleObject(eventInit, INFINITE);
			++wdata.myIndex;
		}
	}

	ifile.close();

	for (auto h : threads) {
		WaitForSingleObject(h, INFINITE);
		CloseHandle(h);
	}

	CloseHandle(mutex);
	CloseHandle(eventInit);

	cout << "Result: " << csequence << endl;
	ofstream ofile("output.txt");
	ofile << csequence;
	ofile.close();

	return 0;
}
