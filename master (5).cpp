#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <cmath>
#include <algorithm>
#include <mpi.h>
#include <fstream>

void CopyData(int *pData, int DataSize, int *pDataCopy);
bool CompareData(int *pData1, int *pData2, int DataSize);
void ProcessInitialization(int *&pData, int &DataSize, int *&pProcData, int &BlockSize);
void DataDistribution(int *pData, int DataSize, int *pProcData, int BlockSize);
void ParallelQuickSort(int *pProcData, int BlockSize);
void DataCollection(int *pData, int DataSize, int *pProcData, int BlockSize);
void TestResult(int *pData, int *pSerialData, int DataSize);
void RandomDataInitialization(int *&pData, int &DataSize);
void ProcessTermination(int *pData, int *pProcData);
void SerialQuickSort(int *l, int *r);
void ExchangeData(int *pProcData, int BlockSize, int DualRank,
				  int *pDualData, int DualBlockSize);

enum split_mode {
	KeepFirstHalf, KeepSecondHalf
};

using namespace std;

int ProcNum = 0;
int ProcRank = -1;

int main(int argc, char *argv[]) {
	int *pData = 0;
	int *pProcData = 0;
	int DataSize = 0;
	int BlockSize = 0;
	int *pSerialData = 0;
	double start, finish;
	double duration = 0.0;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
	MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);

	if (ProcRank == 0) {
		if (argc == 2) {
			DataSize = atoi(argv[1]);
		} else
			return -1;
	}
	ProcessInitialization(pData, DataSize, pProcData, BlockSize);
	if (ProcRank == 0) {
		pSerialData = new int[DataSize];
		CopyData(pData, DataSize, pSerialData);
	}
	start = MPI_Wtime();
	DataDistribution(pData, DataSize, pProcData, BlockSize);
	ParallelQuickSort(pProcData, BlockSize);
	DataCollection(pData, DataSize, pProcData, BlockSize);

	finish = MPI_Wtime();

	TestResult(pData, pSerialData, DataSize);
	
	duration = finish - start;
	if (ProcRank == 0) {
		printf("Time of execution: %f\n", duration);
		ofstream ofile("output.txt");
		ofile << duration;
		ofile.close();
	}
	if (ProcRank == 0)
		delete[]pSerialData;
	ProcessTermination(pData, pProcData);
	MPI_Finalize();
	return 0;
}

void ProcessInitialization(int *&pData, int &DataSize, int *&pProcData, int &BlockSize) {
	setvbuf(stdout, 0, _IONBF, 0);
	if (ProcRank == 0) {
		printf("Sorting %d data items\n", DataSize);
	}
	MPI_Bcast(&DataSize, 1, MPI_INT, 0, MPI_COMM_WORLD);
	int RestData = DataSize;
	for (int i = 0; i < ProcRank; i++)
		RestData -= RestData / (ProcNum - i);
	BlockSize = RestData / (ProcNum - ProcRank);
	pProcData = new int[BlockSize];
	if (ProcRank == 0) {
		pData = new int[DataSize];
		RandomDataInitialization(pData, DataSize);
	}
}

void ProcessTermination(int *pData, int *pProcData) {
	if (ProcRank == 0)
		delete[]pData;
	delete[]pProcData;
}

void DataDistribution(int *pData, int DataSize, int *pProcData, int BlockSize) {
	int *pSendInd = new int[ProcNum];
	int *pSendNum = new int[ProcNum];
	int RestData = DataSize;
	int CurrentSize = DataSize / ProcNum;
	pSendNum[0] = CurrentSize;
	pSendInd[0] = 0;
	for (int i = 1; i < ProcNum; i++) {
		RestData -= CurrentSize;
		CurrentSize = RestData / (ProcNum - i);
		pSendNum[i] = CurrentSize;
		pSendInd[i] = pSendInd[i - 1] + pSendNum[i - 1];
	}
	MPI_Scatterv(pData, pSendNum, pSendInd, MPI_INT, pProcData,
				 pSendNum[ProcRank], MPI_INT, 0, MPI_COMM_WORLD);
	delete[] pSendNum;
	delete[] pSendInd;
}

void DataCollection(int *pData, int DataSize, int *pProcData, int BlockSize) {
	int *pReceiveNum = new int[ProcNum];
	int *pReceiveInd = new int[ProcNum];
	int RestData = DataSize;
	pReceiveInd[0] = 0;
	pReceiveNum[0] = DataSize / ProcNum;
	for (int i = 1; i < ProcNum; i++) {
		RestData -= pReceiveNum[i - 1];
		pReceiveNum[i] = RestData / (ProcNum - i);
		pReceiveInd[i] = pReceiveInd[i - 1] + pReceiveNum[i - 1];
	}
	MPI_Gatherv(pProcData, BlockSize, MPI_INT, pData,
				pReceiveNum, pReceiveInd, MPI_INT, 0, MPI_COMM_WORLD);
	delete[]pReceiveNum;
	delete[]pReceiveInd;
}

void ParallelQuickSort(int *pProcData, int BlockSize) {
	SerialQuickSort(pProcData, pProcData + BlockSize);
	int Offset;
	split_mode SplitMode;
	for (int i = 0; i < ProcNum; i++) {
		if ((i % 2) == 1) {
			if ((ProcRank % 2) == 1) {
				Offset = 1;
				SplitMode = KeepFirstHalf;
			} else {
				Offset = -1;
				SplitMode = KeepSecondHalf;
			}
		} else {
			if ((ProcRank % 2) == 1) {
				Offset = -1;
				SplitMode = KeepSecondHalf;
			} else {
				Offset = 1;
				SplitMode = KeepFirstHalf;
			}
		}
		if ((ProcRank == ProcNum - 1) && (Offset == 1)) continue;
		if ((ProcRank == 0) && (Offset == -1)) continue;
		MPI_Status status;
		int DualBlockSize;
		MPI_Sendrecv(&BlockSize, 1, MPI_INT, ProcRank + Offset, 0,
					 &DualBlockSize, 1, MPI_INT, ProcRank + Offset, 0,
					 MPI_COMM_WORLD, &status);
		int *pDualData = new int[DualBlockSize];
		int *pMergedData = new int[BlockSize + DualBlockSize];
		ExchangeData(pProcData, BlockSize, ProcRank + Offset, pDualData, DualBlockSize);
		merge(pProcData, pProcData + BlockSize, pDualData, pDualData + DualBlockSize, pMergedData);
		if (SplitMode == KeepFirstHalf)
			copy(pMergedData, pMergedData + BlockSize, pProcData);
		else
			copy(pMergedData + BlockSize, pMergedData + BlockSize + DualBlockSize, pProcData);
		delete[]pDualData;
		delete[]pMergedData;
	}
}

void ExchangeData(int *pProcData, int BlockSize, int DualRank,
				  int *pDualData, int DualBlockSize) {
	MPI_Status status;
	MPI_Sendrecv(pProcData, BlockSize, MPI_INT, DualRank, 0,
				 pDualData, DualBlockSize, MPI_INT, DualRank, 0,
				 MPI_COMM_WORLD, &status);
}

void TestResult(int *pData, int *pSerialData, int DataSize) {
	MPI_Barrier(MPI_COMM_WORLD);
	if (ProcRank == 0) {
		SerialQuickSort(pSerialData, pSerialData + DataSize);
		if (!CompareData(pData, pSerialData, DataSize))
			printf("The results of serial and parallel algorithms are "
				   "NOT identical. Check your code\n");
		else
			printf("The results of serial and parallel algorithms are "
				   "identical\n");
	}
}

void RandomDataInitialization(int *&pData, int &DataSize) {
	srand((unsigned)time(0));
	for (int i = 0; i < DataSize; i++)
		pData[i] = rand();
}

void CopyData(int *pData, int DataSize, int *pDataCopy) {
	copy(pData, pData + DataSize, pDataCopy);
}

bool CompareData(int *pData1, int *pData2, int DataSize) {
	return equal(pData1, pData1 + DataSize, pData2);
}

void SerialQuickSort(int *l, int *r) {
	if (r - l <= 1) return;
	int z = *(l + (r - l) / 2);
	int *ll = l, *rr = r - 1;
	while (ll <= rr) { //O(n), попеременный сдвиг с концов навстречу
		while (*ll < z) ll++; //Часть цикла O(n)
		while (*rr > z) rr--; //Часть цикла O(n)
		if (ll <= rr) {
			swap(*ll, *rr);
			ll++;
			rr--;
		}
	}
	if (l < rr) SerialQuickSort(l, rr + 1); //Рекурсия, O(f(n))
	if (ll < r) SerialQuickSort(ll, r); //Рекурсия, O(f(n))
}